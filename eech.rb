#!/home/hz/.rbenv/shims/ruby

module Enumerable
  def eech
    self.length.times do |x|
      yield(self[x])
    end
    return self
  end
end
