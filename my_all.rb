#!/home/hz/.rbenv/shims/ruby

require './eech.rb'

module Enumerable
  def my_all?
    retr = []
    self.each do |x|
      if yield(x)
        puts "Succeeded: #{x}"
        retr.push(self[x])
      else
        puts "Failed: #{x}"
      end
    end
    return retr.length == self.length ? true : false
  end
end

arr = [3,4]
puts arr.my_all? { |x| x >= 3 }
puts arr.my_all? { |x| x <= 3 }
